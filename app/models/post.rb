class Post < ApplicationRecord
  validates :title, presence: true, length: {minimum:5, maximum: 10, too_long: "%{count} characters is the maximum allowed", too_short: "%{count} characters is the minimum allowed"}
  validates :description, presence: true, length: {minimum:10, maximum: 500, too_long: "%{count} characters is the maximum allowed", too_short: "%{count} characters is the minimum allowed"}
  validates :keywords, presence: true, length: {minimum:2, maximum: 10, too_long: "%{count} characters is the maximum allowed", too_short: "%{count} characters is the minimum allowed"}
  has_many_attached :images

end

